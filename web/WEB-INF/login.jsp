<%-- 
    Document   : login.jsp
    Created on : 25 oct. 2018, 11:27:00
    Author     : Lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />   
         <link rel="stylesheet" type="text/css" href="../css/main.css" />  
    </head>
    <body>
        <div class="container">
            <br/>
            <h3>Gestion des employés</h3>
            <br/>
            <strong style="color:red;">${error}</strong>
            <br/><br/>
            <form  action="login" method="post">
                <div class="form-group">
                  <label >Identifiant</label>
                  <input type="text" class="form-control" placeholder="Entrez votre identifiant" name="username">
                </div>
                <div class="form-group">
                  <label >Mot de passe</label>
                  <input type="password" class="form-control" name="password" placeholder="Entrez votre mot de passe">
                </div>
                <input type="submit" class="btn btn-primary" value="Connexion"/>
            </form>
            
        </div>
    </body>
</html>
