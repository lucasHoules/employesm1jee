package efrei.jeeproject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Contient les strings des messages d'erreurs ou de succès
 * @author Lucas
 */
public class Constantes {
    
    public static String MESSAGE_EMPLOYEE_EMPTY = "<p style='color:red'>Nous devons recruter !</p>";
    
    public static String MESSAGE_DELETE_SUCCESS = "<p style='color:blue'>La suppression a réussi !</p>";
    public static String MESSAGE_DELETE_FAILURE = "<p style='color:red'>La suppression a échoué !</p>";
    public static String MESSAGE_DELETE_FIELD_MISSING = "<p style='color:red'>Veuillez sélectionner l'employé à supprimer !</p>";
    
    public static String MESSAGE_AUTH_FIELD_MISSING = "Veuillez saisir un identifiant et un mot de passe";
    public static String MESSAGE_AUTH_FAILURE = "Infos de connexion non valides. Réessayez svp";
    
    public static String MESSAGE_ADD_EMPLOYEE_FIELD_MISSING = "Échec de l'ajout. Veuillez remplir tous les champs svp.";
    public static String MESSAGE_ADD_EMPLOYEE_FAILURE = "L'insertion de l'employé a échoué ! Une des contraites de la table n'a pas été respecté.";
    
    public static String MESSAGE_UPDATE_SHOW_EMPLOYEE_FIELD_MISSING = "<p style='color:red'>Veuillez sélectionner l'employé à modifier !</p>";
    public static String MESSAGE_UPDATE_EMPLOYEE_FIELD_MISSING = "Veuillez remplir tous les champs !";
}
