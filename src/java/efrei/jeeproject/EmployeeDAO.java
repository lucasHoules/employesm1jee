/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package efrei.jeeproject;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Classe héritant du DAO avec le type Employee
 * @author Quentin Lathaud
 */
public class EmployeeDAO extends DAO<Employee> {

    /**
     * Réalise une recherche par id en bdd et retourne l'employe
     * @param employe Employé à trouver
     * @return L'employé trouvé dans la recherche
     */
    @Override
    public Employee find(Employee employe) {        
        
          try {	
            PreparedStatement prepare = this.connexionSQL
                                        .prepareStatement(
                                            "SELECT * FROM EMPLOYES WHERE id = ?"
                                        );
                    prepare.setLong(1, employe.getId());        

                    ResultSet resultats = prepare.executeQuery();
                    employe = hydrate(resultats, employe);
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
          
          return employe;
    }

    /**
     * Créé un employé dans la bdd
     * @param employe Prend l'objet Employee à ajouter 
     * @return L'employé venant d'être inséré en bdd
     * @throws java.sql.SQLException
     */
    @Override
    public Employee create(Employee employe) throws SQLException {
     
        PreparedStatement prepare = this.connexionSQL.prepareStatement(
                                    "INSERT INTO EMPLOYES(nom,prenom,teldom,telport,telpro,adresse,codepostal,ville,email) VALUES(?,?,?,?,?,?,?,?,?)"
                                );
            prepare = preparingStatement(prepare, employe);
            prepare.executeUpdate();	
            
	    return find(employe);
    }

    /**
     * Met à jour un employé
     * @param employe Employé déjà modifié, prêt à être modifié en bdd
     * @return Renvoie l'employé modifié
     */
    @Override
    public Employee update(Employee employe) {
        try {	
            PreparedStatement prepare = this.connexionSQL
                                        .prepareStatement(
                                            "UPDATE EMPLOYES SET nom = ?, prenom = ?, teldom = ?, telport = ?, telpro = ?, adresse = ?, codepostal = ?, ville = ?, email = ? WHERE id = ?"
                                        );
                    prepare = preparingStatement(prepare, employe);
                    prepare.setInt(10, employe.getId());
                    prepare.executeUpdate();	
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	    return employe;
    }

    /**
     * Supprime l'employé via son Id en bdd
     * @param employee Employé à supprimer
     * @return Renvoie un booléen en fonction de la réussite ou l'échec de la suppression
     */
    @Override
    public boolean delete(Employee employee) {
        int resultats;
        try {	
            PreparedStatement prepare = this.connexionSQL
                    .prepareStatement("DELETE FROM EMPLOYES WHERE id = ?");
                prepare.setInt(1, employee.getId());        

                resultats = prepare.executeUpdate();
                
                if(resultats > 0){
                    return true;
                }
	} catch (SQLException e) {
	    e.printStackTrace(); 
	}
        return false;
    }
    
    /**
     * Remplit un objet employé depuis le résultat d'une requête SQL
     * @param resultats Résultats de la requête SQL
     * @param employe Objet Employé à remplir en fonction de la requête
     * @return Employé remplit via la requête
     */
    private Employee hydrate(ResultSet resultats, Employee employee){
        try{
            while(resultats.next()){
                employee.setId(resultats.getInt("ID"));
                employee.setAdresse(resultats.getString("ADRESSE"));
                employee.setCodepostal(resultats.getString("CODEPOSTAL"));
                employee.setNom(resultats.getString("NOM"));
                employee.setPrenom(resultats.getString("PRENOM"));
                employee.setTeldom(resultats.getString("TELDOM"));
                employee.setTelport(resultats.getString("TELPORT"));
                employee.setTelpro(resultats.getString("TELPRO"));
                employee.setEmail(resultats.getString("EMAIL"));
                employee.setVille(resultats.getString("VILLE"));
            }
        } catch(SQLException e){
             e.printStackTrace();
        }
        return employee;
    }

    /**
     * Fonction préparant la requête en fonction de l'employé passé
     * @param ps La requête à remplir
     * @param employee employé ciblé par la requête
     * @return la requête remplie
     * @throws SQLException Erreur lors du remplissage de la requête
     */
    private PreparedStatement preparingStatement(PreparedStatement ps, Employee employee) throws SQLException{
        ps.setString(1, employee.getNom());
        ps.setString(2, employee.getPrenom());
        ps.setString(3, employee.getTeldom());
        ps.setString(4, employee.getTelport());
        ps.setString(5, employee.getTelpro());
        ps.setString(6, employee.getAdresse());
        ps.setString(7, employee.getCodepostal());
        ps.setString(8, employee.getVille());
        ps.setString(9, employee.getEmail());
        return ps;
    }
}
