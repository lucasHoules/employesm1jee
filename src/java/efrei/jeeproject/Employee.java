package efrei.jeeproject;

import javax.servlet.http.HttpServletRequest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Classe représentant un employé
 * @author Lucas
 */
public class Employee {
    
    private int id;
    private String nom;
    private String prenom;
    private String teldom;
    private String telport;
    private String telpro;
    private String adresse;
    private String codepostal;
    private String ville;
    private String email;
    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
   
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the teldom
     */
    public String getTeldom() {
        return teldom;
    }

    /**
     * @param teldom the teldom to set
     */
    public void setTeldom(String teldom) {
        this.teldom = teldom;
    }

    /**
     * @return the telport
     */
    public String getTelport() {
        return telport;
    }

    /**
     * @param telport the telport to set
     */
    public void setTelport(String telport) {
        this.telport = telport;
    }

    /**
     * @return the telpro
     */
    public String getTelpro() {
        return telpro;
    }

    /**
     * @param telpro the telpro to set
     */
    public void setTelpro(String telpro) {
        this.telpro = telpro;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the codepostal
     */
    public String getCodepostal() {
        return codepostal;
    }

    /**
     * @param codepostal the codepostal to set
     */
    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Vérifie que la requête possède tous ses champs nécessaires à construire l'employé
     * @param request Requête http
     * @return Renvoie un booléen en fonction du résultat du test
     */
    public static boolean isEmployeeComplet (HttpServletRequest request){
        return !(request.getParameter("nom").isEmpty() || request.getParameter("prenom").isEmpty() || request.getParameter("teldom").isEmpty() ||request.getParameter("telmob").isEmpty() || request.getParameter("telpro").isEmpty() ||request.getParameter("email").isEmpty() || request.getParameter("codepostal").isEmpty() || request.getParameter("adresse").isEmpty() || request.getParameter("ville").isEmpty());
    }
}
