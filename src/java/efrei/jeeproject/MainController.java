package efrei.jeeproject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Controleur principal de l'applicatio, hérite du servlet
 * @author Lucas
 */
@WebServlet(urlPatterns = {"/"})
public class MainController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            
            switch(request.getServletPath()){
            case "/login":
                this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
                request.getSession().removeAttribute("IS_AUTHENTICATED");
                break;
                
            case "/":
                if(request.getSession().getAttribute("IS_AUTHENTICATED") != null){          
                    ArrayList <Employee> employes = new ArrayList();

                    Connection connexion = null;
                    try {
                        connexion = SGBD.getInstance();
                        Statement statement = connexion.createStatement();
                        ResultSet resultats = statement.executeQuery( "SELECT * FROM EMPLOYES" );
                        boolean dbHasEmployees = false;

                        while(resultats.next()){  
                          dbHasEmployees = true;
                          Employee employe = new Employee();
                          employe.setPrenom(resultats.getString("PRENOM"));
                          employe.setNom(resultats.getString("NOM"));
                          employe.setTeldom(resultats.getString("TELDOM"));
                          employe.setTelport(resultats.getString("TELPORT"));
                          employe.setTelpro(resultats.getString("TELPRO"));
                          employe.setAdresse(resultats.getString("ADRESSE"));
                          employe.setCodepostal(resultats.getString("CODEPOSTAL"));
                          employe.setVille(resultats.getString("VILLE"));
                          employe.setEmail(resultats.getString("EMAIL"));
                          employe.setId(Integer.valueOf(resultats.getString("ID")));

                          employes.add(employe);
                          request.setAttribute("employes", employes);  
                        }
                        
                        if(!dbHasEmployees){
                            request.setAttribute("emptyList",Constantes.MESSAGE_EMPLOYEE_EMPTY);
                        }
                    
                    } catch (SQLException e) {   
                          System.out.println( "Erreur lors de la connexion :  <br/>"
                            + e.getMessage() );
                    } finally {               
                        if (connexion != null) {
                            try {                  
                                connexion.close();
                            } catch (SQLException e) {
                                System.out.println( "Erreur lors de la fermeture de la connexion :  <br/>"
                            + e.getMessage() );
                            }
                        }
                }

                  this.getServletContext().getRequestDispatcher("/WEB-INF/resultatsEmployes.jsp").forward(request, response);
                } else{
                    response.sendRedirect(request.getContextPath() + "/login");
                    return;
                }
            break;
        case "/addEmployee":
            this.getServletContext().getRequestDispatcher("/WEB-INF/viewAddEmployee.jsp").forward(request, response);
            break;
            
        case "/disconnect":
            this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
            break;

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        //Vérification que l'utilisateur est authentifié
        if(request.getSession().getAttribute("IS_AUTHENTICATED") != null){        
            switch(request.getServletPath()){
                case "/modifyEmployee":    
                    //Si l'utilisateur ne sélectionne pas de personne à modifier
                    if(request.getParameter("id") == null){
                        request.setAttribute("message",Constantes.MESSAGE_UPDATE_SHOW_EMPLOYEE_FIELD_MISSING);
                        this.getServletContext().getRequestDispatcher("/").forward(request, response);
                    }
                    // Comportement standard
                    else{
                        EmployeeDAO emp_DAO = new EmployeeDAO(); 
                        Employee emp = new Employee();
                        emp.setId(Integer.valueOf(request.getParameter("id")));
                        Employee employeDetails = emp_DAO.find(emp);
                        request.setAttribute("employeDetails", employeDetails);

                    }
                    this.getServletContext().getRequestDispatcher("/WEB-INF/viewModifyEmployee.jsp").forward(request, response);
                   break;

                case "/deleteEmployee":
                    //Si l'utilisateur ne sélectionne pas de personne à supprimer
                    if(request.getParameter("id") == null){
                        request.setAttribute("message",Constantes.MESSAGE_DELETE_FAILURE);
                        this.getServletContext().getRequestDispatcher("/").forward(request, response);
                    }
                    //Comportement standard
                    else{
                        EmployeeDAO empDAO = new EmployeeDAO(); 
                        Employee emp = new Employee();
                        emp.setId(Integer.valueOf(request.getParameter("id")));
                        if(empDAO.delete(emp)){
                            request.setAttribute("message",Constantes.MESSAGE_DELETE_SUCCESS);
                        }
                        //else{ request.setAttribute("deleteState",Constantes.MESSAGE_DELETE_FAILURE); } //Supprimé via le nouvel énoncé

                        this.getServletContext().getRequestDispatcher("/").forward(request, response);

                    }
                    break;
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        switch(request.getServletPath()){
            case "/login":
            //Récupération des données du POST
            String usernameEnter = request.getParameter("username");
            String passwordEnter = request.getParameter("password");
            
            //Création de l'utilisateur en conséquence
            User user = new User();
            user.setUsername(usernameEnter);
            user.setPassword(passwordEnter);

            request.setAttribute("user", user);
           
            String error = "";
            
            try {
                UserDAO userDAO = new UserDAO();
                
                //Si l'utilisateur existe
                if(userDAO.checkUser(user)){ 
                    //On l'authentifie
                    HttpSession session = request.getSession();
                    session.setAttribute("IS_AUTHENTICATED", true);
                    
                    //On le redirige vers la page de la liste des employés
                    response.sendRedirect(request.getContextPath() + "/");
                    return;
                }
                else{
                    //Autrement on attribue une erreur
                    error = Constantes.MESSAGE_AUTH_FAILURE;                                
                }                   
            
            }catch (SQLException e) {   
                  System.out.println( "Erreur lors de la connexion :  <br/>"
                    + e.getMessage() );
            }
                 
            //Si un des deux champs n'est pas rempli on attribut l'erreur adéquat
            if(usernameEnter.isEmpty() || passwordEnter.isEmpty()){
                error = Constantes.MESSAGE_AUTH_FIELD_MISSING;
            }
            
            //On envoie l'erreur et renvoie sur login
            request.setAttribute("error", error);   
            processRequest(request, response);
            break;
            
        case "/recordEmployee":
            String errorAddEmp ="";
            
            //Si l'utilisateur est authentifié
            if(request.getSession().getAttribute("IS_AUTHENTICATED") != null){     
                //Si tous les champs n'ont été remplis
                if(!Employee.isEmployeeComplet(request)){
                    //On les redirige vers la page d'ajout avec un message d'erreur
                    errorAddEmp= Constantes.MESSAGE_ADD_EMPLOYEE_FIELD_MISSING;
                    request.setAttribute("error", errorAddEmp);   
                    this.getServletContext().getRequestDispatcher("/WEB-INF/viewAddEmployee.jsp").forward(request, response);
                }
                //Autrement on ajoute l'employé
                else{
                    Employee employe = new Employee();
                    hydrate(request, employe); //Remplit l'objet Employé avec la requête
                    EmployeeDAO ed = new EmployeeDAO();
                    try{
                        ed.create(employe);
                    }
                    catch(SQLException e){
                        //En cas d'erreur, on redirige vers la vue d'ajout avec un message d'erreur
                        error = Constantes.MESSAGE_ADD_EMPLOYEE_FAILURE;
                        request.setAttribute("error", error);   
                        this.getServletContext().getRequestDispatcher("/WEB-INF/viewAddEmployee.jsp").forward(request, response);
                    }
                    response.sendRedirect(request.getContextPath() + "/");
                }
            }
            //S'il n'est pas authentifié on le redirige vers la page de login
            else{
                response.sendRedirect(request.getContextPath() + "/login");
                return;
            }
            break;
                      
        case "/updatingEmployee":
        //Si l'utilisateur est authentifié
        if(request.getSession().getAttribute("IS_AUTHENTICATED") != null){
            EmployeeDAO empDAO = new EmployeeDAO();
            Employee updateEmploye = new Employee();
            updateEmploye = hydrate(request, updateEmploye);
            updateEmploye.setId(Integer.parseInt(request.getParameter("id")));
            empDAO.update(updateEmploye);
            response.sendRedirect(request.getContextPath() + "/");
         }
        //Sinon on le redirige vers la page d'accueil
         else{
             response.sendRedirect(request.getContextPath() + "/login");
             return;
         }
         break;
        
        case "/disconnect":
            //Si la personne était connecté, on la déconnecte et la redirige vers la page aurevoir
            if(request.getSession().getAttribute("IS_AUTHENTICATED") != null){
                request.getSession().setAttribute("IS_AUTHENTICATED",null);
                this.getServletContext().getRequestDispatcher("/WEB-INF/goodbye.jsp").forward(request, response);
            }
            //Sinon on la redirige vers la page login
            else{
                response.sendRedirect(request.getContextPath() + "/login");
                return;
            }
            break;
            
        default :
            processRequest(request,response);
        }
       
    }

       /**
     * Remplit un objet employé depuis le résultat d'une requête HTTP
     * @param request Requête HTTP
     * @param employe Objet Employé à remplir en fonction de la requête
     * @return Employé remplit via la requête
     */
    private Employee hydrate(HttpServletRequest request, Employee employe){
        employe.setEmail(request.getParameter("email"));
        employe.setNom(request.getParameter("nom"));
        employe.setPrenom(request.getParameter("prenom"));
        employe.setTeldom(request.getParameter("teldom"));
        employe.setTelport(request.getParameter("telmob"));
        employe.setTelpro(request.getParameter("telpro"));
        employe.setAdresse(request.getParameter("adresse"));
        employe.setCodepostal(request.getParameter("codepostal"));
        employe.setVille(request.getParameter("ville"));
        return employe;
    }
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
