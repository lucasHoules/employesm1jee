/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package efrei.jeeproject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe permettant d'instancier la connexion à la base de données (bdd)
 * @author Quentin Lathaud
 */
public class SGBD {
    
    private static Connection connexion;
    
    /**
     * @return instance de connexion à la bdd
     */
    public static Connection getInstance(){
        System.out.println("Loading driver...");
        
        //Récupération du driver JDBC Driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded!");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }
        
        //Récupération de identifiants de connexion à la base ainsi que son URI
        Properties prop = new Properties();
        InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("efrei/jeeproject/db.properties");
        try {
            prop.load(input);
        } catch (IOException ex) {
            Logger.getLogger(SGBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String url = prop.getProperty("url");
        String username = prop.getProperty("username");
        String password = prop.getProperty("password");

        System.out.println("Connecting database...");
        
        //Tentative de connexion
        try {
            connexion = DriverManager.getConnection(url, username, password);
            System.out.println("Database connected!");
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
        
        return connexion;
    }
}
