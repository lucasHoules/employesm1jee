/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package efrei.jeeproject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe héritant de la classe DAO avec le type User
 * @author Quentin Lathaud
 */
public class UserDAO extends DAO<User> {

    /**
     * Vérifie si l'utilisateur existe
     * @param utilisateur Utilisateur à vérifier
     * @return Existance de l'objet via un booléen
     * @throws SQLException Passe l'erreur SQL lié à la requête
     */
    public boolean checkUser(User utilisateur) throws SQLException  {
        PreparedStatement prepare = this.connexionSQL.prepareStatement("SELECT * FROM identifiants WHERE LOGIN = ? AND MDP = ?");
        prepare.setString(1, utilisateur.getUsername());        
        prepare.setString(2, utilisateur.getPassword());
        ResultSet resultats = prepare.executeQuery();
        if(resultats.next()){       
            return true;
        } else{
            return false;
        }
    }

    /**
     * Créer l'utilisateur
     * @param obj utilisateur créer
     * @return 
     * @throws SQLException Pas encore géré
     */
    @Override
    public User create(User obj) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Met à jour l'utilisateur
     * @param obj utilisateur à mettre à jour
     * @return Utilisateur mis à jour
     */
    @Override
    public User update(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Supprime l'utilisateur
     * @param obj utilisateur à supprimer
     * @return Suppression de l'objet selon un booléen
     */
    @Override
    public boolean delete(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Trouve un utilisateur
     * @param obj Utilisateur à trouver
     * @return renvoie l'utilisateur trouvé
     * @throws SQLException Exception car pas implémenté
     */
    @Override
    public User find(User obj) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
