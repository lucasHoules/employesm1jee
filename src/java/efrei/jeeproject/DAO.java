/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package efrei.jeeproject;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Permet l'abstraction de l'aspect métier avec la base de données
 * @author pc-benoit
 * @param <T> Type de la classe qui implémente la classe DAO
 */
public abstract class DAO<T> {

    /**
     * Objet représentant la connexion avec la base de données
     */
    public Connection connexionSQL = SGBD.getInstance();
	
	/**
	 * Permet de récupérer une instance en base de l'objet fournit en paramètre
	 * @param obj Objet à trouver
	 * @return Renvoie l'objet du type du DAO implémenté
         * @throws java.sql.SQLException
	 */
	public abstract T find(T obj) throws SQLException;
	
	/**
	 * Permet de créer une entrée dans la base de données par rapport à un objet
	 * @param obj Objet à créer
         * @return Renvoie l'objet du type du DAO implémenté
     * @throws java.sql.SQLException
	 */
	public abstract T create(T obj) throws SQLException;
	
	/**
	 * Permet de mettre à jour les données d'une entrée dans la base
	 * @param obj Objet à mettre à jour
         * @return Renvoie l'objet du type du DAO implémenté
	 */
	public abstract T update(T obj);
	
	/**
	 * Permet la suppression d'une entrée de la base
         * @param obj Objet à supprimer
         * @return Renvoie vrai si l'action réussi, faux si l'action échoue
	 */
	public abstract boolean delete(T obj);
}

